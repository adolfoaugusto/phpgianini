<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <table border="1">
            <tr>
                <td align="center"> <b>Nomes</b></td>
            </tr>
            <?php
            $nomes = array("adolfo", "augusto", "rodrigues");
            foreach($nomes as $nome):
                echo '<tr>'.'<td>'. $nome .'</td>'.'</tr>';
            endforeach;
            ?>
        </table><br>
        <table border="1">
            <tr>
                <td align="center"> <b>Nomes 2</b></td>
            </tr>
            <?php
            $nomes = array("adolfo", "augusto", "rodrigues");
            foreach($nomes as $nome):
            ?>
                <tr>
                    <td><?php echo $nome ?></td>
                </tr>
                <?php
            endforeach;
            ?>
        </table>
    
    </body>
</html>
